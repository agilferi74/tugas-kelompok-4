<?php

define("BASE_URL", "http://localhost/tugas-kelompok-4/");
$helper = new Helper();
$helper->connection();

class Helper
{
    public function connection()
    {
        $server = "localhost";
        $username = "root";
        $password = "";
        $dbname = "gudang";

        $conn = mysqli_connect($server, $username, $password, $dbname) or die("Koneksi ke database gagal");
        return $conn;
    }
}
