<?php
require_once('helper/helper.php');

class PenggunaController
{
    public function fetchPengguna()
    {
        $helper = new Helper();
        $pengguna = mysqli_query($helper->connection(), "SELECT * FROM `pengguna`, `hak_akses` WHERE pengguna.id_akses = hak_akses.id_akses");
        return $pengguna;
    }
}
