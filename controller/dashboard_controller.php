<?php
require_once('helper/helper.php');

class DashboardController
{
    public function countPenjualan()
    {
        $helper = new Helper();
        $countPenjualan = mysqli_query($helper->connection(), "SELECT COUNT(*) as jumlah FROM `penjualan`");
        $data = mysqli_fetch_assoc($countPenjualan);
        return $data['jumlah'];
    }

    public function countPembelian()
    {
        $helper = new Helper();
        $countPembelian = mysqli_query($helper->connection(), "SELECT COUNT(*) as jumlah FROM `pembelian`");
        $data = mysqli_fetch_assoc($countPembelian);
        return $data['jumlah'];
    }

    public function labaRugi()
    {
        $helper = new Helper();
        $queryPenjualan = mysqli_query($helper->connection(), "SELECT SUM(harga_jual * jumlah_penjualan) as penjualan FROM penjualan");
        $dataPenjualan = mysqli_fetch_assoc($queryPenjualan);
        $queryPembelian = mysqli_query($helper->connection(), "SELECT SUM(jumlah_pembelian * harga_beli) as pembelian FROM `pembelian`");
        $dataPembelian = mysqli_fetch_assoc($queryPembelian);
        return (int)$dataPenjualan['penjualan'] - (int)$dataPembelian['pembelian'];
    }
}
