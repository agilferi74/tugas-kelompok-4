<?php
require_once('helper/helper.php');

class PenjualanController
{
    public function fetchPenjualan()
    {
        $helper = new Helper();
        $penjualan = mysqli_query($helper->connection(), "SELECT * FROM `penjualan`, `barang` WHERE penjualan.id_barang = barang.id_barang");
        return $penjualan;
    }
}
