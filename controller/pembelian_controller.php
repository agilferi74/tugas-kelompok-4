<?php
require_once('helper/helper.php');

class PembelianController
{
    public function fetchPembelian()
    {
        $helper = new Helper();
        $pembelian = mysqli_query($helper->connection(), "SELECT * FROM `pembelian`, `barang` WHERE pembelian.id_pembelian = barang.id_barang");
        return $pembelian;
    }
}
