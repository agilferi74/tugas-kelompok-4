<?php
require_once('helper/helper.php');

class HakAksessController
{
    public function fetchHakAkses()
    {
        $helper = new Helper();
        $hak_akses = mysqli_query($helper->connection(), "SELECT * FROM hak_akses");
        return $hak_akses;
    }
}
