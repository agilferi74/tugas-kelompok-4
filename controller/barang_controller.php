<?php
require_once('helper/helper.php');

class BarangController
{
    public function fetchBarang()
    {
        $helper = new Helper();
        $barang = mysqli_query($helper->connection(), "SELECT * FROM barang, pengguna WHERE barang.id_pengguna = pengguna.id_pengguna");
        return $barang;
    }

    public function findByIdBarang($id)
    {
        $helper = new Helper();
        $hak_akses = mysqli_query($helper->connection(), "SELECT * FROM barang WHERE id_barang = $id");
        $query = mysqli_fetch_assoc($hak_akses);
        return $query;
    }

    public function editBarang($id, $namaBarang, $keterangan, $satuan, $idPengguna)
    {
        $helper = new Helper();
        $query = "UPDATE barang SET nama_barang = '$namaBarang', keterangan = '$keterangan', satuan = '$satuan', id_pengguna = $idPengguna WHERE id_barang = '$id'";
        $update = mysqli_query($helper->connection(), $query);
        return $update;
    }

    public function addBarang($namaBarang, $keterangan, $satuan, $idPengguna)
    {
        $helper = new Helper();
        $query = "INSERT INTO barang (nama_barang, keterangan, satuan, id_pengguna) VALUES ('$namaBarang', '$keterangan', '$satuan', '$idPengguna')";
        $insert = mysqli_query($helper->connection(), $query);
        return $insert;
    }

    public function deletebarang($id)
    {
        $helper = new Helper();
        $query = "DELETE FROM barang WHERE id_barang = '$id'";
        $delete = mysqli_query($helper->connection(), $query);
        return $delete;
    }
}
