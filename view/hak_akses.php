<?php
require('controller/hak_akses_controller.php');
$hakAkses = new HakAksessController();
$hak_akses = $hakAkses->fetchHakAkses();
?>

<div class="card">
    <div class=" card-body">
        <div class="row">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Akses</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($hak_akses as $p) : ?>
                            <tr>
                                <th scope="row"><?= $no++; ?></th>
                                <td><?= $p['nama_akses'] ?></td>
                                <td><?= $p['keterangan'] ?></td>
                                <td>
                                    <form method="POST" action="">
                                        <input type="hidden" value="<?= $p['id_akses'] ?>" name="id_akses">
                                        <button type="submit" name="btn-delete" class="btn btn-success">Hapus</button>
                                    </form>
                                    <a class="btn btn-info badge" href="">Edit</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>