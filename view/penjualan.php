<?php
require('controller/penjualan_controller.php');
$penjualan = new PenjualanController();
$data = $penjualan->fetchPenjualan();

?>

<div class="card" style="margin : 16px">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Barang</th>
                            <th scope="col">Jumlah Pembelian</th>
                            <th scope="col">Harga Beli</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data as $p) : ?>
                            <tr>
                                <th scope="row"><?= $no++; ?></th>
                                <td><?= $p['nama_barang'] ?></td>
                                <td><?= $p['jumlah_penjualan'] ?></td>
                                <td><?= $p['harga_jual'] ?></td>
                                <td>
                                    <a class="btn btn-danger badge">Delete</a>
                                    <a class="btn btn-info badge">Edit</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>