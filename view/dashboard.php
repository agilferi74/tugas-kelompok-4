<?php
require('controller/dashboard_controller.php');
$dashboard = new DashboardController();
$penjualan = $dashboard->countPenjualan();
$pembelian = $dashboard->countPembelian();
$labaRugi = $dashboard->labaRugi();
$laba = 0;
$rugi = 0;

if ($labaRugi < 0) {
    $rugi = abs($labaRugi);
} else {
    $laba = $labaRugi;
}

?>

<h1 class="mt-4">Dashboard</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Dashboard</li>
</ol>
<div class="row">
    <div class="col-xl-3 col-md-6">
        <div class="card bg-primary text-white mb-4">
            <div class="card-body">Jumlah Penjualan</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link"><?= $penjualan ?></a>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card bg-warning text-white mb-4">
            <div class="card-body">Jumlah Pembelian</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link"><?= $pembelian ?></a>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card bg-success text-white mb-4">
            <div class="card-body">Laba</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link"><?= $laba ?></a>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card bg-danger text-white mb-4">
            <div class="card-body">Rugi</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link"><?= $rugi ?></a>
            </div>
        </div>
    </div>
</div>