<?php
require('controller/barang_controller.php');
$barang = new BarangController();
$data = $barang->fetchBarang();

if (isset($_POST['btn-delete'])) {
    $idBarang = $_POST['id_barang'];
    $delete = $barang->deletebarang($idBarang);
    if ($delete) {
        header('location: index.php?page=barang');
    } else {
        echo "<div class='alert alert-danger'>Gagal Hapus Data!!!</div>";
    }
}
?>

<h1 class="mt-4">Barang</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Barang</li>
</ol>
<a class="btn btn-info badge" href="<?= BASE_URL . 'index.php?page=edit_barang' ?>">Tambah</a>
<div class="card" style="margin : 16px">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">Satuan</th>
                            <th scope="col">Nama Pengguna</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data as $p) : ?>
                            <tr>
                                <th scope="row"><?= $no++; ?></th>
                                <td><?= $p['nama_barang'] ?></td>
                                <td><?= $p['keterangan'] ?></td>
                                <td><?= $p['satuan'] ?></td>
                                <td><?= $p['nama_pengguna'] ?></td>
                                <td>
                                    <form method="POST" action="">
                                        <input type="hidden" value="<?= $p['id_barang'] ?>" name="id_barang">
                                        <button type="submit" name="btn-delete" class="btn btn-success">Hapus</button>
                                    </form>
                                    <a class="btn btn-info badge" href="<?= BASE_URL . 'index.php?page=edit_barang&id=' . $p['id_barang'] ?>">Edit</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>