<?php
require('controller/barang_controller.php');

$id = isset($_GET['id']) ? ($_GET['id']) : false;

$barang = new BarangController();
$namaBarang = "";
$keterangan = "";
$satuan = "";
if ($id != null) {
    $hak_akses = $barang->findByIdBarang($id);
    $namaBarang = $hak_akses["nama_barang"];
    $keterangan = $hak_akses["keterangan"];
    $satuan = $hak_akses["satuan"];
}


if (isset($_POST['btn-simpan'])) {
    $inputNamaAkses = $_POST['nama_barang'];
    $inputKeterangan = $_POST['keterangan'];
    $inputSatuan = $_POST['satuan'];
    $idPengguna = $_POST['id_pengguna'];
    $query = false;
    if ($id != null) {
        $query = $barang->editbarang($id, $inputNamaAkses, $inputKeterangan, $inputSatuan, $idPengguna);
    } else {
        $query = $barang->addbarang($inputNamaAkses, $inputKeterangan, $inputSatuan, $idPengguna);
    }
    if ($query) {
        header('location: index.php?page=barang');
    } else {
        echo "<div class='alert alert-danger'>Gagal Edit Data!!!</div>";
    }
}

?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card shadow-lg border-0 rounded-lg mt-5">
                <div class="card-header">
                    <h3 class="text-center font-weight-light my-4">Edit Hak Akses</h3>
                </div>
                <div class="card-body">
                    <form method="POST" action="">
                        <div class="form-group">
                            <label class="small mb-1" for="nama_barang">Nama Barang</label>
                            <input class="form-control py-4" id="nama_barang" placeholder="Nama Barang" name="nama_barang" value="<?= $namaBarang ?>" />
                        </div>
                        <div class="form-group">
                            <label class="small mb-1" for="keterangan">Keterangan</label>
                            <input class="form-control py-4" id="keterangan" placeholder="Keterangan" name="keterangan" value="<?= $keterangan ?>" />
                        </div>
                        <div class="form-group">
                            <label class="small mb-1" for="keterangan">Satuan</label>
                            <input class="form-control py-4" id="satuan" placeholder="Satuan" name="satuan" value="<?= $satuan ?>" />
                        </div>
                        <input type="hidden" value="<?= 4 ?>" name="id_pengguna">
                        <button type="submit" name="btn-simpan" class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>