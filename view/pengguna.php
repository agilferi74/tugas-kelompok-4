<?php
require('controller/pengguna_controller.php');
$pengguna = new PenggunaController();
$hak_akses = $pengguna->fetchPengguna();

?>

<div class="card" style="margin : 16px">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Pengguna</th>
                            <th scope="col">Nama Depan</th>
                            <th scope="col">Nama Belakang</th>
                            <th scope="col">No HP</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Nama Akses</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($hak_akses as $p) : ?>
                            <tr>
                                <th scope="row"><?= $no++; ?></th>
                                <td><?= $p['nama_pengguna'] ?></td>
                                <td><?= $p['nama_depan'] ?></td>
                                <td><?= $p['nama_belakang'] ?></td>
                                <td><?= $p['no_hp'] ?></td>
                                <td><?= $p['alamat'] ?></td>
                                <td><?= $p['nama_akses'] ?></td>
                                <td>
                                    <a class="btn btn-danger badge">Delete</a>
                                    <a class="btn btn-info badge">Edit</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>